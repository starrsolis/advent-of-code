def open_input(input_path: str) -> list[str]:
    """Opens and sanitizes the `input.txt` file for the day reading in each line as an element of a list.
    - Remove any extra lines at the end of the input files (if present, due to auto-formatting)
    - Remove newline characters for individual element strings
    - Strip any whitespace
    """
    with open(input_path, 'r') as f:
        lines = f.readlines()

    line_data = lines[:-1] if lines[-1] == '' else lines
    data = [line_item.replace('\n', '').strip() for line_item in line_data]

    return data


def main():
    data = open_input('input.txt')
    answer_1, answer_2 = get_answer(data)

    print(answer_1)
    print(answer_2)

    return answer_1, answer_2


def get_answer(data):
    """
    - Calories
    - Split by empty lines per elf
    Prompt Q1: which elf has the most calories
    Prompt Q2: Which 3 elfs have the most combined
    """
    calories_per_elf = []
    elf_calories = 0
    for entry in data:
        if entry == '':
            calories_per_elf.append(elf_calories)
            elf_calories = 0
        else:
            elf_calories += int(entry)

    answer_1 = max(calories_per_elf)
    answer_2 = sum(sorted(calories_per_elf, reverse=True)[:3])

    return answer_1, answer_2


if __name__ == '__main__':
    main()
